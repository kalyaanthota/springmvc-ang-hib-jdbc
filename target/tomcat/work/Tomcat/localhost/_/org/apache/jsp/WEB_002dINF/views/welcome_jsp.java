/*
 * Generated by the Jasper component of Apache Tomcat
 * Version: Apache Tomcat/7.0.47
 * Generated at: 2017-10-26 13:41:10 UTC
 * Note: The last modified time of this file was set to
 *       the last modified time of the source file after
 *       generation to assist with modification tracking.
 */
package org.apache.jsp.WEB_002dINF.views;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class welcome_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final javax.servlet.jsp.JspFactory _jspxFactory =
          javax.servlet.jsp.JspFactory.getDefaultFactory();

  private static java.util.Map<java.lang.String,java.lang.Long> _jspx_dependants;

  static {
    _jspx_dependants = new java.util.HashMap<java.lang.String,java.lang.Long>(3);
    _jspx_dependants.put("/WEB-INF/views/common/navigation.jspf", Long.valueOf(1509023768218L));
    _jspx_dependants.put("/WEB-INF/views/common/footer.jspf", Long.valueOf(1508846409454L));
    _jspx_dependants.put("/WEB-INF/views/common/header.jspf", Long.valueOf(1508935111303L));
  }

  private org.apache.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005fspring_005fmessage_0026_005fcode_005fnobody;

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.tomcat.InstanceManager _jsp_instancemanager;

  public java.util.Map<java.lang.String,java.lang.Long> getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _005fjspx_005ftagPool_005fspring_005fmessage_0026_005fcode_005fnobody = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_instancemanager = org.apache.jasper.runtime.InstanceManagerFactory.getInstanceManager(getServletConfig());
  }

  public void _jspDestroy() {
    _005fjspx_005ftagPool_005fspring_005fmessage_0026_005fcode_005fnobody.release();
  }

  public void _jspService(final javax.servlet.http.HttpServletRequest request, final javax.servlet.http.HttpServletResponse response)
        throws java.io.IOException, javax.servlet.ServletException {

    final javax.servlet.jsp.PageContext pageContext;
    javax.servlet.http.HttpSession session = null;
    final javax.servlet.ServletContext application;
    final javax.servlet.ServletConfig config;
    javax.servlet.jsp.JspWriter out = null;
    final java.lang.Object page = this;
    javax.servlet.jsp.JspWriter _jspx_out = null;
    javax.servlet.jsp.PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<html>\n");
      out.write("<head>\n");
      out.write("<title>Todos Application</title>\n");
      out.write("<script src=\"webjars/jquery/1.9.1/jquery.min.js\"></script>\n");
      out.write("<!-- <link href=\"webjars/bootstrap/3.3.6/css/bootstrap.min.css\" rel=\"stylesheet\" media=\"screen\">\n");
      out.write("\t\n");
      out.write("<script src=\"webjars/bootstrap/3.3.6/js/bootstrap.min.js\"></script>\n");
      out.write("<script\n");
      out.write("\tsrc=\"webjars/bootstrap-datepicker/1.0.1/js/bootstrap-datepicker.js\"></script> -->\n");
      out.write("<!-- Latest compiled and minified CSS -->\n");
      out.write("<link rel=\"stylesheet\" href=\"//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css\">\n");
      out.write("\n");
      out.write("<!-- Optional theme -->\n");
      out.write("<link rel=\"stylesheet\" href=\"//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css\">\n");
      out.write("\n");
      out.write("<!-- Latest compiled and minified JavaScript -->\n");
      out.write("<script src=\"//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js\"></script>\t\n");
      out.write("<style type=\"text/css\">\n");
      out.write(".footer-bottom {\n");
      out.write("    \n");
      out.write("    min-height: 45px;\n");
      out.write("    width: 100%;\n");
      out.write("}\n");
      out.write(".copyright {\n");
      out.write("    color: #fff;\n");
      out.write("    line-height: 30px;\n");
      out.write("    min-height: 30px;\n");
      out.write("    padding: 7px 0;\n");
      out.write("}\n");
      out.write(".design {\n");
      out.write("    color: #fff;\n");
      out.write("    line-height: 30px;\n");
      out.write("    min-height: 30px;\n");
      out.write("    padding: 7px 0;\n");
      out.write("    text-align: right;\n");
      out.write("}\n");
      out.write(".design a {\n");
      out.write("    color: #fff;\n");
      out.write("}\n");
      out.write(".payments {\n");
      out.write("\tfont-size: 1.5em;\t\n");
      out.write("}\n");
      out.write("              input.hidden {\n");
      out.write("    position: absolute;\n");
      out.write("    left: -9999px;\n");
      out.write("}\n");
      out.write("\n");
      out.write("#profile-image1 {\n");
      out.write("    cursor: pointer;\n");
      out.write("  \n");
      out.write("     width: 150px;\n");
      out.write("    height: 250px;\n");
      out.write("\tborder:2px solid #03b1ce ;}\n");
      out.write("\t.tital{ font-size:16px; font-weight:500;}\n");
      out.write("\t .bot-border{ border-bottom:1px #696969 solid;  margin:5px 0  5px 0}\t\n");
      out.write("</style>\n");
      out.write("</head>\n");
      out.write("\n");
      out.write("<body>\n");
      out.write("\n");
      out.write('\n');
      out.write("   \n");
      out.write("<nav role=\"navigation\" class=\"navbar navbar-inverse\">\n");
      out.write("\n");
      out.write("\t<div class=\"\">\n");
      out.write("\t\t<a href=\"http://www.kalyan.com\" class=\"navbar-brand\">kalyan</a>\n");
      out.write("\t</div>\n");
      out.write("\n");
      out.write("\t<div class=\"navbar-collapse\">\n");
      out.write("\t\t<ul class=\"nav navbar-nav\">\n");
      out.write("\t\t\t<li class=\"active\"><a href=\"/\">Home</a></li>\n");
      out.write("\t\t\t<li><a href=\"/list-todos\">Todos</a></li>\n");
      out.write("\t\t</ul>\n");
      out.write("\t\t<ul class=\"nav navbar-nav navbar-right\">\n");
      out.write("\t\t\t<li><a href=\"/logout\">Logout</a></li>\n");
      out.write("\t\t</ul>\n");
      out.write("\t</div>\n");
      out.write("\n");
      out.write("</nav>");
      out.write("\n");
      out.write("\n");
      out.write("<script src=\"https://ajax.googleapis.com/ajax/libs/angularjs/1.0.8/angular.min.js\"></script>\n");
      out.write("    \t<script>\n");
      out.write("    \tfunction Hello($scope, $http) {\n");
      out.write("    \t    $http.get('http://localhost:8080/users/springcontent.json').\n");
      out.write("    \t        success(function(data) {\n");
      out.write("    \t            $scope.user = data;\n");
      out.write("\t          });\n");
      out.write("    \t}\n");
      out.write("    \t</script>\n");
      out.write("<div class=\"container\">\n");
      out.write("\t");
      if (_jspx_meth_spring_005fmessage_005f0(_jspx_page_context))
        return;
      out.write(" <kbd>");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${name}", java.lang.String.class, (javax.servlet.jsp.PageContext)_jspx_page_context, null, false));
      out.write(".</kbd>\n");
      out.write("</div>\n");
      out.write("<div class=\"clearfix\"></div>\n");
      out.write("<div class=\"pull-right \">\n");
      out.write("\n");
      out.write("<h2>\n");
      out.write("        <a href=\"users/page\" style=\"margin-right: 50px;\" class=\"btn btn-info btn-rounded btn-lg\">Click to USERS</a>\n");
      out.write("    </h2>\n");
      out.write("</div>\n");
      out.write("<div class=\"container\">\n");
      out.write("\t<div class=\"row\">\n");
      out.write("       <div class=\"col-md-7 \">\n");
      out.write("\n");
      out.write("<div class=\"panel panel-default\">\n");
      out.write("  <div class=\"panel-heading\">  <h4 >User Profile :</h4></div>\n");
      out.write("   <div class=\"panel-body\">\n");
      out.write("       \n");
      out.write("    <div class=\"box box-info\" ng-app=\"\" ng-controller=\"Hello\">\n");
      out.write("        \n");
      out.write("            <div class=\"box-body\">\n");
      out.write("                     <div class=\"col-sm-6 col-sm-6\">\n");
      out.write("                     <div  align=\"center\"> <img alt=\"User Pic\" src=\"https://x1.xingassets.com/assets/frontend_minified/img/users/nobody_m.original.jpg\" id=\"profile-image1\" class=\"img-circle img-responsive\"> \n");
      out.write("                \n");
      out.write("                <input id=\"profile-image-upload\" class=\"hidden\" type=\"file\">\n");
      out.write("\t\t\t<div style=\"color:#999;\" >click image to change profile image</div>\n");
      out.write("                <!--Upload Image Js And Css-->\n");
      out.write("                     </div>\n");
      out.write("              <br>\n");
      out.write("              <!-- /input-group -->\n");
      out.write("            </div>\n");
      out.write("            <div class=\"col-sm-6\">\n");
      out.write("            <h3 style=\"color:#00b1b1;\"><b>{{user.username}} </b></h3></span>\n");
      out.write("              <span><p>Aspirant</p></span>\n");
      out.write("              <hr style=\"margin:5px 0 5px 0; border-bottom:3px #696969 solid;\">            \n");
      out.write("            <div class=\"bot-border\"></div>\n");
      out.write("            <div class=\"col-sm-6 col-xs-3 tital \" >User Name:</div><div class=\"col-sm-1 col-xs-2 \" style=\"color:#00b1b1;\">{{user.username}}</div>\n");
      out.write("            <div class=\"clearfix\"></div>\n");
      out.write("            <div class=\"bot-border\"></div>\n");
      out.write("            <div class=\"col-sm-6 col-xs-3 tital \" >Password:</div><div class=\"col-sm-1 col-xs-2 \" style=\"color:#00b1b1;\">{{user.password}}</div>\n");
      out.write("            <div class=\"clearfix\"></div>\n");
      out.write("            <div class=\"bot-border\"></div>\n");
      out.write("            <div class=\"col-sm-6 col-xs-3 tital \" >EMail Id:</div><div class=\"col-sm-1 col-xs-2 \" style=\"color:#00b1b1;\">{{user.email}}</div>\n");
      out.write("            <div class=\"clearfix\"></div>\n");
      out.write("            <div class=\"bot-border\"></div>\n");
      out.write("            <div class=\"col-sm-6 col-xs-3 tital \" >Create Time:</div><div class=\"col-sm-1 col-xs-2 \" style=\"color:#00b1b1;\">{{user.createtime}}</div>\n");
      out.write("            <div class=\"clearfix\"></div>\n");
      out.write("            <div class=\"bot-border\"></div>\n");
      out.write("            <div class=\"col-sm-6 col-xs-3 tital \" >User Id:</div><div class=\"col-sm-1 col-xs-2 \" style=\"color:#00b1b1;\">{{user.userid}}</div>\n");
      out.write("            <div class=\"clearfix\"></div>\n");
      out.write("            <div class=\"bot-border\"></div>\n");
      out.write("            <hr style=\"margin:5px 0 5px 0; border-bottom:3px #696969 solid;\">\n");
      out.write("    </div>\n");
      out.write("            <!-- /.box-body -->\n");
      out.write("          </div>\n");
      out.write("          <!-- /.box -->\n");
      out.write("        </div>\n");
      out.write("    </div> \n");
      out.write("    </div>\n");
      out.write("</div>\n");
      out.write("\n");
      out.write("\n");
      out.write("<div class=\"footer navbar-fixed-bottom navbar-inverse\">\n");
      out.write("\n");
      out.write("<div class=\"footer-bottom\">\n");
      out.write("\t<div class=\"container\">\n");
      out.write("\t\t<div class=\"row\">\n");
      out.write("\t\t\t<div class=\"col-xs-12 col-sm-6 col-md-6 col-lg-6\">\n");
      out.write("\t\t\t\t<div class=\"copyright\">\n");
      out.write("\t\t\t\t\t© 2017 :: CalyKrish.Inc, All rights reserved\n");
      out.write("\t\t\t\t</div>\n");
      out.write("\t\t\t</div>\n");
      out.write("\t\t\t<div class=\"col-xs-12 col-sm-6 col-md-6 col-lg-6\">\n");
      out.write("\t\t\t\t<div class=\"design\">\n");
      out.write("\t\t\t\t\n");
      out.write("             ||  <a target=\"_blank\" href=\"http://www.google.com\">Web Design & Development by Kalyan Thota</a>\n");
      out.write("\t\t\t\t</div>\n");
      out.write("\t\t\t</div>\n");
      out.write("\t\t</div>\n");
      out.write("\t</div>\n");
      out.write("\n");
      out.write("</div>\n");
      out.write("</body>\n");
      out.write("</html>");
      out.write("  \n");
      out.write("    <script>\n");
      out.write("              $(function() {\n");
      out.write("    $('#profile-image1').on('click', function() {\n");
      out.write("        $('#profile-image-upload').click();\n");
      out.write("    });\n");
      out.write("});       \n");
      out.write(" </div>\n");
      out.write("</div>\n");
      out.write("\n");
    } catch (java.lang.Throwable t) {
      if (!(t instanceof javax.servlet.jsp.SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }

  private boolean _jspx_meth_spring_005fmessage_005f0(javax.servlet.jsp.PageContext _jspx_page_context)
          throws java.lang.Throwable {
    javax.servlet.jsp.PageContext pageContext = _jspx_page_context;
    javax.servlet.jsp.JspWriter out = _jspx_page_context.getOut();
    //  spring:message
    org.springframework.web.servlet.tags.MessageTag _jspx_th_spring_005fmessage_005f0 = (org.springframework.web.servlet.tags.MessageTag) _005fjspx_005ftagPool_005fspring_005fmessage_0026_005fcode_005fnobody.get(org.springframework.web.servlet.tags.MessageTag.class);
    _jspx_th_spring_005fmessage_005f0.setPageContext(_jspx_page_context);
    _jspx_th_spring_005fmessage_005f0.setParent(null);
    // /WEB-INF/views/welcome.jsp(14,1) name = code type = null reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_spring_005fmessage_005f0.setCode("welcome.message");
    int[] _jspx_push_body_count_spring_005fmessage_005f0 = new int[] { 0 };
    try {
      int _jspx_eval_spring_005fmessage_005f0 = _jspx_th_spring_005fmessage_005f0.doStartTag();
      if (_jspx_th_spring_005fmessage_005f0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        return true;
      }
    } catch (java.lang.Throwable _jspx_exception) {
      while (_jspx_push_body_count_spring_005fmessage_005f0[0]-- > 0)
        out = _jspx_page_context.popBody();
      _jspx_th_spring_005fmessage_005f0.doCatch(_jspx_exception);
    } finally {
      _jspx_th_spring_005fmessage_005f0.doFinally();
      _005fjspx_005ftagPool_005fspring_005fmessage_0026_005fcode_005fnobody.reuse(_jspx_th_spring_005fmessage_005f0);
    }
    return false;
  }
}
