/*
 * Generated by the Jasper component of Apache Tomcat
 * Version: Apache Tomcat/7.0.47
 * Generated at: 2017-10-26 13:41:10 UTC
 * Note: The last modified time of this file was set to
 *       the last modified time of the source file after
 *       generation to assist with modification tracking.
 */
package org.apache.jsp.WEB_002dINF.views;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class error_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final javax.servlet.jsp.JspFactory _jspxFactory =
          javax.servlet.jsp.JspFactory.getDefaultFactory();

  private static java.util.Map<java.lang.String,java.lang.Long> _jspx_dependants;

  static {
    _jspx_dependants = new java.util.HashMap<java.lang.String,java.lang.Long>(3);
    _jspx_dependants.put("/WEB-INF/views/common/navigation.jspf", Long.valueOf(1509023768218L));
    _jspx_dependants.put("/WEB-INF/views/common/footer.jspf", Long.valueOf(1508846409454L));
    _jspx_dependants.put("/WEB-INF/views/common/header.jspf", Long.valueOf(1508935111303L));
  }

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.tomcat.InstanceManager _jsp_instancemanager;

  public java.util.Map<java.lang.String,java.lang.Long> getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_instancemanager = org.apache.jasper.runtime.InstanceManagerFactory.getInstanceManager(getServletConfig());
  }

  public void _jspDestroy() {
  }

  public void _jspService(final javax.servlet.http.HttpServletRequest request, final javax.servlet.http.HttpServletResponse response)
        throws java.io.IOException, javax.servlet.ServletException {

    final javax.servlet.jsp.PageContext pageContext;
    javax.servlet.http.HttpSession session = null;
    final javax.servlet.ServletContext application;
    final javax.servlet.ServletConfig config;
    javax.servlet.jsp.JspWriter out = null;
    final java.lang.Object page = this;
    javax.servlet.jsp.JspWriter _jspx_out = null;
    javax.servlet.jsp.PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<html>\n");
      out.write("<head>\n");
      out.write("<title>Todos Application</title>\n");
      out.write("<script src=\"webjars/jquery/1.9.1/jquery.min.js\"></script>\n");
      out.write("<!-- <link href=\"webjars/bootstrap/3.3.6/css/bootstrap.min.css\" rel=\"stylesheet\" media=\"screen\">\n");
      out.write("\t\n");
      out.write("<script src=\"webjars/bootstrap/3.3.6/js/bootstrap.min.js\"></script>\n");
      out.write("<script\n");
      out.write("\tsrc=\"webjars/bootstrap-datepicker/1.0.1/js/bootstrap-datepicker.js\"></script> -->\n");
      out.write("<!-- Latest compiled and minified CSS -->\n");
      out.write("<link rel=\"stylesheet\" href=\"//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css\">\n");
      out.write("\n");
      out.write("<!-- Optional theme -->\n");
      out.write("<link rel=\"stylesheet\" href=\"//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css\">\n");
      out.write("\n");
      out.write("<!-- Latest compiled and minified JavaScript -->\n");
      out.write("<script src=\"//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js\"></script>\t\n");
      out.write("<style type=\"text/css\">\n");
      out.write(".footer-bottom {\n");
      out.write("    \n");
      out.write("    min-height: 45px;\n");
      out.write("    width: 100%;\n");
      out.write("}\n");
      out.write(".copyright {\n");
      out.write("    color: #fff;\n");
      out.write("    line-height: 30px;\n");
      out.write("    min-height: 30px;\n");
      out.write("    padding: 7px 0;\n");
      out.write("}\n");
      out.write(".design {\n");
      out.write("    color: #fff;\n");
      out.write("    line-height: 30px;\n");
      out.write("    min-height: 30px;\n");
      out.write("    padding: 7px 0;\n");
      out.write("    text-align: right;\n");
      out.write("}\n");
      out.write(".design a {\n");
      out.write("    color: #fff;\n");
      out.write("}\n");
      out.write(".payments {\n");
      out.write("\tfont-size: 1.5em;\t\n");
      out.write("}\n");
      out.write("              input.hidden {\n");
      out.write("    position: absolute;\n");
      out.write("    left: -9999px;\n");
      out.write("}\n");
      out.write("\n");
      out.write("#profile-image1 {\n");
      out.write("    cursor: pointer;\n");
      out.write("  \n");
      out.write("     width: 150px;\n");
      out.write("    height: 250px;\n");
      out.write("\tborder:2px solid #03b1ce ;}\n");
      out.write("\t.tital{ font-size:16px; font-weight:500;}\n");
      out.write("\t .bot-border{ border-bottom:1px #696969 solid;  margin:5px 0  5px 0}\t\n");
      out.write("</style>\n");
      out.write("</head>\n");
      out.write("\n");
      out.write("<body>\n");
      out.write("\n");
      out.write('\n');
      out.write("   \n");
      out.write("<nav role=\"navigation\" class=\"navbar navbar-inverse\">\n");
      out.write("\n");
      out.write("\t<div class=\"\">\n");
      out.write("\t\t<a href=\"http://www.kalyan.com\" class=\"navbar-brand\">kalyan</a>\n");
      out.write("\t</div>\n");
      out.write("\n");
      out.write("\t<div class=\"navbar-collapse\">\n");
      out.write("\t\t<ul class=\"nav navbar-nav\">\n");
      out.write("\t\t\t<li class=\"active\"><a href=\"/\">Home</a></li>\n");
      out.write("\t\t\t<li><a href=\"/list-todos\">Todos</a></li>\n");
      out.write("\t\t</ul>\n");
      out.write("\t\t<ul class=\"nav navbar-nav navbar-right\">\n");
      out.write("\t\t\t<li><a href=\"/logout\">Logout</a></li>\n");
      out.write("\t\t</ul>\n");
      out.write("\t</div>\n");
      out.write("\n");
      out.write("</nav>");
      out.write("\n");
      out.write("<div class=\"container\">\n");
      out.write("\t<img src=\"https://thumbs.dreamstime.com/x/error-d-people-upset-metaphor-43976249.jpg\" /> Application has encountered an error. Please contact support on ...\n");
      out.write("</div>\n");
      out.write("<div class=\"footer navbar-fixed-bottom navbar-inverse\">\n");
      out.write("\n");
      out.write("<div class=\"footer-bottom\">\n");
      out.write("\t<div class=\"container\">\n");
      out.write("\t\t<div class=\"row\">\n");
      out.write("\t\t\t<div class=\"col-xs-12 col-sm-6 col-md-6 col-lg-6\">\n");
      out.write("\t\t\t\t<div class=\"copyright\">\n");
      out.write("\t\t\t\t\t© 2017 :: CalyKrish.Inc, All rights reserved\n");
      out.write("\t\t\t\t</div>\n");
      out.write("\t\t\t</div>\n");
      out.write("\t\t\t<div class=\"col-xs-12 col-sm-6 col-md-6 col-lg-6\">\n");
      out.write("\t\t\t\t<div class=\"design\">\n");
      out.write("\t\t\t\t\n");
      out.write("             ||  <a target=\"_blank\" href=\"http://www.google.com\">Web Design & Development by Kalyan Thota</a>\n");
      out.write("\t\t\t\t</div>\n");
      out.write("\t\t\t</div>\n");
      out.write("\t\t</div>\n");
      out.write("\t</div>\n");
      out.write("\n");
      out.write("</div>\n");
      out.write("</body>\n");
      out.write("</html>");
      out.write('\n');
    } catch (java.lang.Throwable t) {
      if (!(t instanceof javax.servlet.jsp.SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
